#include "royalexecutioner.h"

RoyalExecutioner::RoyalExecutioner()
{

}

void RoyalExecutioner::executeCalculator(QString input)
{
    try
    {
        QList<Token> tokenList;
        Parser parser(input);
        tokenList = parser.getTokenList();
        Lexer lexer(tokenList);
        tokenList = lexer.lexerWork();
        Calculator calculator(tokenList);
        double result = calculator.calculateThings(dictionary);
        qInfo() << "Result: " << result;
    } catch (QString error){
        qInfo() << error;
    }
}
