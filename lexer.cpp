#include "lexer.h"


Lexer::Lexer(QList<Token> tokenList)
{
    tokenList_ = tokenList;
}

QList<Token> Lexer::lexerWork()
{
    bool isValid = true;
    if (tokenList_.contains(Token(Token::Type::assignment,"="))){
        isValid = reworkAssignment();
    }
    if (!isValid){
        tokenList_.clear();
        return tokenList_;
    }
    isValid = checkBrackets();
    if (!isValid){
        tokenList_.clear();
        return tokenList_;
    }
    isValid = reworkOperators();
    if (!isValid){
        tokenList_.clear();
        return tokenList_;
    }
    tokenList_ = sortToRpn();
    return tokenList_;
}

QList<Token> Lexer::sortToRpn()
{
    QList<Token> tokenListRpn;
    QList<Token> operatorsStack;
    for (int i = 0; i < tokenList_.length(); i++){
        if (tokenList_[i].readType() == Token::Type::assignment){
            operatorsStack.append(tokenList_[i]);//! Присвоение кладется на стек операторов
            continue;
        }
        if (tokenList_[i].readType() == Token::Type::constant){
            tokenListRpn.append(tokenList_[i]);//! Константа кладется в выходной список
            if (!operatorsStack.isEmpty()){
                if (operatorsStack.last().readType() == Token::Type::unaryMinus){
                    tokenListRpn.append(operatorsStack.last());//! Если на стеке операторов
                    operatorsStack.pop_back();                 //! унарный минус - положить
                }                                              //! его в выходную строку
            }
            continue;
        }
        if (tokenList_[i].readType() == Token::Type::definition){
            tokenListRpn.append(tokenList_[i]);//! Новая переменная кладется в выходной список
            continue;
        }
        if (tokenList_[i].readType() == Token::Type::unaryMinus){
            operatorsStack.append(tokenList_[i]);//! Унарный минус кладется в стек операторов
            continue;
        }
        if (tokenList_[i].readType() == Token::Type::variable){
            tokenListRpn.append(tokenList_[i]);//! Переменная кладется в выходной список
            if (!operatorsStack.isEmpty()){
                if (operatorsStack.last().readType() == Token::Type::unaryMinus){
                    tokenListRpn.append(operatorsStack.last());//! Если на стеке операторов
                    operatorsStack.pop_back();                 //! унарный минус - положить
                }                                              //! его в выходную строку
            }
            continue;
        }
        if (tokenList_[i].readType() == Token::Type::operation){
            if (tokenList_[i].readValue().contains(QRegExp("[/*]"))){
                operatorsStack.append(tokenList_[i]);//! Умножение или деление просто
                continue;                            //! кладется в стек операторов
            }
            if (tokenList_[i].readValue().contains(QRegExp("[+-]"))){
                while ((!operatorsStack.isEmpty())&& (operatorsStack.last().readValue().contains(QRegExp("[/*]")))){
                    tokenListRpn.append(operatorsStack.last());
                    operatorsStack.pop_back();
                    //! В случае сложения и вычитания сначала выкидываем из стека все
                    //! операции более высокого приоритета, потом +- кладется в стек
                }
                operatorsStack.append(tokenList_[i]);
                continue;
            }
        }
        if (tokenList_[i].readType() == Token::Type::bracket){
            if (tokenList_[i].readValue() == "("){    //! Открывающаяся скобка просто
                operatorsStack.append(tokenList_[i]); //! кладется в стек операторов
                continue;
            } else {
                while (operatorsStack.last().readValue() != "("){
                    tokenListRpn.append(operatorsStack.last()); //! Добавляем все знаки
                    operatorsStack.pop_back();                  //! до откр. скобки
                }
                operatorsStack.pop_back();//! Удаляем из стека саму откр. скобку
                if (operatorsStack.last().readType() == Token::Type::unaryMinus){
                    tokenListRpn.append(operatorsStack.last()); //! Если до скобки унарный
                    operatorsStack.pop_back();                  //! минус - добавляем и его
                }
                continue;
            }
        }
        qInfo() << "WTF IS THAT TOKEN??\n";
    }
    //! Теперь оставшиеся операторы вбрасываем в выходную строку
    while (!operatorsStack.isEmpty()){
        tokenListRpn.append(operatorsStack.last());
        operatorsStack.pop_back();
    }
    return tokenListRpn;
}

bool Lexer::checkBrackets()
{
    int openBrackets = 0;
    foreach (Token tok, tokenList_){
        if (tok.readType() == Token::Type::bracket){
            if (tok.readValue() == "("){
                ++openBrackets;
            } else {
                --openBrackets;
            }
        }
        if (openBrackets < 0){
            throw QString("Corrupt closing bracket ");
            return false;
        }
    }
    if (openBrackets == 0){
        return true;
    } else {
        throw QString("Unclosed bracket ");
        return false;
    }
}

bool Lexer::reworkOperators()
{
    int i = tokenList_.indexOf(Token(Token::Type::assignment,"="));
    ++i;
    if (tokenList_[i].readType() == Token::Type::operation){
        //! Отдельная обработка первого элемента выражения.
        //! Это либо первый элемент вообще, либо следующий за присвоением
        if (tokenList_[i].readValue() != "-"){
            throw QString("Corrupt operators ");
            return false;
        }
        if (tokenList_[i + 1].readType() == Token::Type::operation){
            throw QString("Corrupt operators ");
            return false;
        }
        tokenList_.removeAt(i);
        tokenList_.insert(i,Token(Token::Type::unaryMinus,"-"));
    }
    if (tokenList_.last().readType() == Token::Type::operation){
        //! Отдельная обработка последнего элемента
        //! Если ее не сделать, выйдем за границы.
        throw QString("Sentence can't end with an operator ");
        return false;
    }

    for (i; i < tokenList_.length(); i++){
        if (tokenList_[i].readType() == Token::Type::operation){
            if (tokenList_[i + 1].readValue() == ")"){//! Если после оператора идет ) - ошибка
                throw QString("Bracket/operation error ");
                return false;
            }
            if ((tokenList_[i - 1].readType() == Token::Type::operation)||
                (tokenList_[i - 1].readValue() == "(")){
                if (tokenList_[i].readValue() != "-"){
                    throw QString("Corrupt operators ");
                    return false;
                }
                if (tokenList_[i + 1].readType() == Token::Type::operation){
                    throw QString("Corrupt operators ");
                    return false;
                }
                tokenList_.removeAt(i);
                tokenList_.insert(i,Token(Token::Type::unaryMinus,"-"));
            }
        }
    }
    return true;
}

bool Lexer::reworkAssignment()
{
    if (tokenList_.indexOf(Token(Token::Type::assignment, "=")) != 1){
        //! Если оператор присвоения стоит не вторым - ошибка
        throw QString("Assignment error ");
        return false;
    }
    if (tokenList_.count(Token(Token::Type::assignment,"=")) > 1){
        //! Если операторов присвоения более одного - ошибка
        throw QString("Too many assignments ");
        return false;
    }
    if (tokenList_[0].readType() != Token::Type::variable){
        //! Если первый токен - не variable - ошибка
        throw QString("%1 is not a proper name for a variable ").arg(tokenList_[0].readValue());
        return false;
    }
    tokenList_.insert(1,Token(Token::Type::definition,tokenList_[0].readValue()));
    tokenList_.pop_front();
    return true;
}
