#include "calculator.h"

#include <QTextStream>
#include <iostream>
//! Отключать для тестирования!!
QTextStream coutcalc(stdout);
QTextStream cincalc(stdin);



Calculator::Calculator(QList<Token> tokenList)
{
    tokenList_ = tokenList;
}

double Calculator::calculateThings(QMap<QString, double> &dictionary)
{
    QList<double> outputList;       // Стек чисел. Можно было бы оперировать токенами,
                                    // но получаются множественные вызовы типа
                                    // "output.last().readvalue().toDouble()"
    QString varName;                // varName заводится здесь, потому что если завести ее внутри
                                    // foreach, то и жить она будет только внутри foreach.
    foreach (Token tok, tokenList_){
        if (tok.readType() == Token::Type::definition){
            // Была попытка завести ее здесь
            varName = tok.readValue();
            continue;
        }
        if (tok.readType() == Token::Type::variable){
            if (dictionary.contains(tok.readValue())){// Если такая есть - добавляем ее значение
                outputList.append((dictionary.value(tok.readValue())));
                continue;
            } else {                                  // В ином случае - запрашиваем значение,
                                                      // добавляем и в словарь, и в список
                coutcalc<<QString("No variable %1 found \n Please define variable %1=").arg(tok.readValue());
                coutcalc.flush();
//                qInfo() << QString("No variable %1 found \n Please define variable %1=").arg(tok.readValue());
                double input = cincalc.readLine().toDouble();
                dictionary.insert(tok.readValue(),input);
                outputList.append(input);
                continue;
            }
        }
        if (tok.readType() == Token::Type::constant){// Число просто пишется в список
            outputList.append(tok.readValue().toDouble());
            continue;
        }
        if (tok.readType() == Token::Type::unaryMinus){
            double a = outputList.last();            // Унарный минус кидается в стек операторов
            outputList.pop_back();
            outputList.append(-a);
            continue;
        }
        if (tok.readType() == Token::Type::operation){
            double b = outputList.last();// Все бинарные операции обрабатываются одинаково
            outputList.pop_back();       // Берутся два верхних числа
            double a = outputList.last();
            outputList.pop_back();
            if (tok.readValue() == "+"){
                outputList.append(a + b);// Добавляется результат применения к ним операции
            }
            if (tok.readValue() == "-"){
                outputList.append(a - b);
            }
            if (tok.readValue() == "*"){
                outputList.append(a * b);
            }
            if (tok.readValue() == "/"){
                outputList.append(a / b);
            }
            continue;
        }
        if (tok.readType() == Token::Type::assignment){
            double varValue = outputList.last();// По результатам работы лексера получается, что
            dictionary.insert(varName,varValue);// присвоение, если оно есть, идет последним
        }
    }
    if (outputList.size() > 1){
        throw QString("Something went wrong, dunno what ");
        return outputList.last(); // Данная ошибка возникает, если где-то накосячил лексер.
                                  // не калькуляторское это дело - ошибки лексера разбирать
    }
    return outputList.last();
}

