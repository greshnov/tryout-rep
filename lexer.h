#ifndef LEXER_H
#define LEXER_H
#include <QList>
#include <QString>
#include <QStringList>
#include <QDebug>
#include "token.h"

class Lexer//! Занимается проверкой последовательностей и подготовкой к вычислению
{
public:
    Lexer(QList <Token> tokenList);
    QList <Token> lexerWork();
private:
    QList<Token> sortToRpn();//! Сортирует в целях подготовки к ОПН
    bool checkBrackets();//! Проверяет исправность скобок
    bool reworkOperators();//! Проверяет исправность операторов
    bool reworkAssignment();//! Проверяет правильность присвоения
    QList <Token> tokenList_;
};

#endif // LEXER_H
