#ifndef ROYALEXECUTIONER_H
#define ROYALEXECUTIONER_H
#include "calculator.h"
#include "token.h"
#include "parser.h"
#include "lexer.h"
#include "QStringList"
#include "QList"
#include <QMap>


class RoyalExecutioner
{
public:
    RoyalExecutioner();
    void executeCalculator(QString input);
private:
    QMap<QString,double> dictionary;
};

#endif // ROYALEXECUTIONER_H
