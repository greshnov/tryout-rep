#include <QCoreApplication>
#include <QString>
#include <QStringList>
#include <QMap>
#include "token.h"
#include "parser.h"
#include "lexer.h"
#include "calculator.h"
#include "royalexecutioner.h"
#include <QTextStream>
QTextStream coutmain(stdout);
QTextStream cinmain(stdin);

int main(int argc, char *argv[])
{
    RoyalExecutioner execute;
    QString input = "";
    qInfo() << "Enter \"exit\" ot \"qqq\" to stop program execution\n";
    while (1){
        qInfo() << "Enter command:";
        input = cinmain.readLine();
        if ((input == "exit") || (input == "qqq"))
            break;
        if(input.isEmpty())
            continue;
        execute.executeCalculator(input);
    }
    qInfo() << "Exiting...";
    return 0;
}

