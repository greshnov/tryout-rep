#include "parser.h"

Parser::Parser(const QString &str = "")
{
    if (!str.isEmpty()){
        splitToTokenStrings(str); //! Сразу заполняет внутренний QStringList\n
    }
}

QList<Token> Parser::getTokenList()
{
        return parseToTokens();
}

void Parser::splitToTokenStrings(const QString &str) // Делит входную строку на отдельные команды
                                                     // и помещает их во внутренний QStringList

{                                                    // Возможно, данную функцию стоит перенести из
    QString obj;                                     // private в public, чтобы иметь возможность
    foreach (const QChar &c, str){                   // перезадавать прасеру строку. НО ЭТО НЕ ТОЧНО!
        if (c == ' ')                                // Пропускает пробелы. Последовательность
            continue;                                // "aa  12 asd" считается одной строкой "aa12asd"
        if (c == '=' || c == '+' || c == '-' || c == '*'
                || c == '/' || c == '(' || c == ')'){// Перечисление символов, по которым делить
            if (!obj.isEmpty()){
                tokenStrings_.append(obj);
            }
            tokenStrings_.append(c);
            obj.clear();
            continue;
        }
        obj.append(c);
    }
    if (!obj.isEmpty()){
        tokenStrings_.append(obj);
    }
}

QList<Token> Parser::parseToTokens()// Смотрит свой список команд и преобразует в список первичных токенов
{
    foreach (QString str, tokenStrings_){
        bool isDouble;
        str.toDouble(&isDouble);
        if (isDouble){
            tokenList_.append(TokenConstant(str));
            continue;
        }
        if (str == "="){
            tokenList_.append(TokenAssignment(str));// Здесь можно было бы вызывать конструктор без аргумента, но нефиг.
            continue;
        }
        if (str.contains(QRegExp("[-+*/]"))){
            tokenList_.append(TokenOperation(str));
            continue;
        }
        if (str.contains(QRegExp("[)(]"))){
            tokenList_.append(TokenBracket(str));
            continue;
        }
        tokenList_.append(TokenVariable(str));// Все, что не является числом, присвоением, знаком или скобкой,
    }                                         // считается именем переменной.
    return tokenList_;                        // Порядок описания обеспечивает, что выражения типа "22 Е 12" считаются числом.
}

