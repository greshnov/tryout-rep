#ifndef TOKEN_H
#define TOKEN_H
#include <QString>
//! Базовый класс токенов
/*!
 * Один у меня возникает вопрос.
 *  Если нельзя создать виртуальную функцию, которая будет возвращать разные типы, то зачем нужны все эти наследования?\n
 *  Ответ: а чтобы было весело!\n \n
 *  Токены могут быть первичными и вторичными.\n
 *  Первичные - все токены, которые могут возникнуть на этапе парсинга.\n
 *  Вторичные могут возникнуть только при работе лексера.\n
 */
//! ВСЕГО: 5 первичных токенов, 2 вторичных
class Token
{
public:
    enum class Type{
        emptyToken,
        definition,
        variable,
        operation,
        unaryMinus,
        assignment,
        constant,
        bracket
    };
    Token(const Type & _type = Type::emptyToken, const QString &value = "");
    bool operator ==(const Token &) const;
    Type readType() const;
    QString readValue() const;
protected:
    Type tokenType;
    QString tokenValue;
};

//! Определение переменной
/*! Обеспечивает запись результата выражения в Qmap\n
 *  Является вторичным токеном, возникает при наличии tokenAssignment */
class TokenDefinition : public Token
{
public:
    TokenDefinition(const QString &);
};
//! Использование переменной
/*! Все объекты символьного типа по умолчанию считаются variable\n
 *  Является первичным токеном */
class TokenVariable : public Token
{
public:
    TokenVariable(const QString &);
};
//! Операторы
/*! Называется "operation" из-за того, что "operator" - ключевое слово\n
 *  Является первичным токеном */
class TokenOperation : public Token
{
public:
    TokenOperation(const QString &);
};
//! Унарный минус.
/*! ПЛАН: заменяется на (-1)* для вычисления\n
 * ИЗМЕНЕНИЕ: обрабатывается как нормальная унарная операция\n
 *  Является вторичным токеном */
class TokenUnaryMinus : public Token
{
public:
    TokenUnaryMinus(const QString &);
};
//! Знак равенства.
/*! Токены данного типа служат сигналом, что в данном выражении есть определение переменной\n
 *  Является первичным токеном */
class TokenAssignment : public Token
{
public:
    TokenAssignment(const QString &);
};
//! Константные числа
/*! Все, что нормально проходит .toDouble - константные числа\n
 *  Является первичным токеном */
class TokenConstant : public Token
{
public:
    TokenConstant(const QString &);
};
//! Открывающаяся или закрывающаяся скобка.
/*! Пока что один тип токена для обоих скобок. Возможно, потребуется второй\n
 *  Является первичным токеном */
class TokenBracket : public Token
{
public:
    TokenBracket(const QString &);
};

#endif // TOKEN_H
