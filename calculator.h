#ifndef CALCULATOR_H
#define CALCULATOR_H
#include "token.h"
#include <QList>
#include <QDebug>
#include <QStringList>
#include <QMap>

class Calculator//! Выполняет все вычисления на основе ОПН списка токенов
{
public:
    Calculator(QList <Token> tokenList);
    double calculateThings(QMap<QString, double> &dictionary);
private:
    QList <Token> tokenList_;
};

#endif // CALCULATOR_H
