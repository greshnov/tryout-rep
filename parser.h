#ifndef PARSER_H
#define PARSER_H
#include <QString>
#include "token.h"
#include <QStringList>
#include <QList>

class Parser//! Делит входную строку на команды, потом приводит их к первичным токенам
{
public:
    Parser(const QString &);
    QList<Token> getTokenList();
private:
    void splitToTokenStrings(const QString &);
    QList<Token> parseToTokens();
    QStringList tokenStrings_;
    QList<Token> tokenList_;
};

#endif // PARSER_H
