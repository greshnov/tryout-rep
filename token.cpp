#include "token.h"

Token::Token(const Type & _type, const QString &value):tokenType(_type),tokenValue(value)
{
}

bool Token::operator ==(const Token &b) const{
    if (readType() != b.readType())
        return false;
    if (readValue() != b.readValue())
        return false;
    return true;
}

Token::Type Token::readType() const
{
    return tokenType;
}

QString Token::readValue() const
{
    return tokenValue;
}

TokenDefinition::TokenDefinition(const QString &value):Token(Type::definition,value)
{
}

TokenVariable::TokenVariable(const QString &value) : Token(Type::variable,value)
{
}

TokenOperation::TokenOperation(const QString &value) : Token(Type::operation,value)
{
}

TokenUnaryMinus::TokenUnaryMinus(const QString &value = "-") : Token(Type::unaryMinus,value)
{
}

TokenAssignment::TokenAssignment(const QString &value = "=") : Token(Type::assignment,value)
{
}

TokenConstant::TokenConstant(const QString &value) : Token(Type::constant,value)
{
}

TokenBracket::TokenBracket(const QString &value) : Token(Type::bracket,value)
{
}
